const jwt = require('jsonwebtoken');
const express = require('express');
const bcrypt = require('bcryptjs');
const moment = require('moment');
const mongoose = require('mongoose');

const Users = require('../models/users.model.js');

module.exports = {
	register,
	login,
	logout,
	addUser,
	fetchMyUserList,
	updateAddedUsers,
	deleteAddedUsers
}


// Register the user api call
async function register(req,res, next){	
	let username = req.body.username;
	let email = req.body.email;
	let password = req.body.password;
	let	confirm_password = req.body.confirm_password;
	if(await Users.findOne({"username":username}).exec()){
		throw res.json({message:"Username Already registered"});
	}else if(await Users.findOne({"email": email}).exec()){
		throw res.json({message:"Email already registered"});
	}

	let user = new Users(req.body);

	if(req.body.password){
		let password = req.body.password;
	    let salt = await bcrypt.genSalt(10);
	    // console.log(salt);
	    user.hash = bcrypt.hashSync(password, salt);
	    // console.log("user data" + user);
	    // process.exit(1);
	    user.save().then(data=>{
	    	res.json({status: "success", message: username + " yours registeration successfully"})
	    }).catch(err=>{
	    	res.json({status: "failure", message: "Something went wrong", data:err})
	    })
	}
}

// Login api call
async function login(req, res, next){
	console.log(req.body);
	let username = req.body.username;
	let email = req.body.email;
	let password = req.body.password;
	let users = [];

	if(username){
		let user = await Users.findOne({"username": username}).exec();
		users.push(user);
	}else if(email){
		let user = await Users.findOne({"email": email}).exec();
		users.push(user);
	};
	if(!users){
		res.json({status: "failure", message: "User not exits Please register"});
	};
	console.log(users);
	users.forEach((elements)=>{
		let isMatch = bcrypt.compare(password, elements.hash)
		if(!isMatch){
			res.json({status: "failure", message: "Please enter the correct password"});
		};
		const payload = {user: {id: elements.id}};
		console.log(payload);
		jwt.sign(payload,"randomString", {expiresIn: 10000},(err, token) => {
			if (err) throw err;
			let condition = elements.id;
			let update = {"verifytoken": token};
			let options = {multi: true};
			Users.findByIdAndUpdate(condition, update, options).then(data=>{
				res.json({status: "success", message:"login successful",data: data, token: token});
			}).catch(err => {
				res.status(500).send({status: "failure", message: err});
			});
		});
	});
};

// User logout
async function logout(req, res, next){
	let conditions = {'_id': req.body._id}
    var update = { "lastvisited":moment().toDate(),"verifytoken":"","login_status":0};
    var  options = { multi: true };
    await Users.updateOne(conditions, update, options).then(data=>{
		res.json({status: "success", message:"logout successfully"});
	}).catch(err => {
		res.status(500).send({status: "failure", message: err});
	});
}

// Add the User
async function addUser(req, res, next){
	if(!Object.keys(req.body).length){
		return res.status(400).json({status:"failure", message:"Request body can not be empty"});
	}
    const User = new Users(req.body);
    await User.save().then(data=>{
		res.json({status: "success", message:"User add successfully",data:data});
	}).catch(err=>{
		res.status(500).send({status: "failure",message: err});
	});
};

// Fetch the all added users api call
async function fetchMyUserList(req, res, next){
	let query = {'added_by_id': req.params.added_by_id }
	await Users.find(query).then(data=>{
		res.json({status: "success",data:data});
	}).catch(err => {
		res.status(500).send({status: "failure",message: err});
	});
};

// Update the addded users api call
async function updateAddedUsers(req, res, next){
	console.log(req.body);
	let condition = req.body._id;
	let update = req.body.updateObj;
	let options = {multi: true};
	await Users.findByIdAndUpdate(condition, update, options).then(data=>{
		res.json({status: "success", message:"User detail successfully updated"});
	}).catch(err => {
		res.status(500).send({status: "failure", message: err});
	});
};

// Delete the users api call
async function deleteAddedUsers(req, res, next){
	let query = {'_id': req.params._id}
	console.log(query);
	// process.exit(1);
	await Users.deleteOne(query).then(data=>{
		res.json({status: "success", message:"User detail successfully deleted"});
	}).catch(err => {
		res.status(500).send({status: "failure", message: err});
	});
}