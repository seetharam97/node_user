var express = require("express");
const usersController = require('../controller/users.controller.js');

var router = express.Router();
// routes(app);

router.post('/register', usersController.register);
router.post('/login', usersController.login);
router.post('/logout', usersController.logout);
router.post('/addUser', usersController.addUser);
router.get('/fetchMyUserList/:added_by_id', usersController.fetchMyUserList);
router.put('/updateAddedUsers', usersController.updateAddedUsers);
router.delete('/deleteAddedUsers/:_id', usersController.deleteAddedUsers);

module.exports = router;