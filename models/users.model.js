const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
	username: {type: String, required: false},
	name: { type: String, required: false},
	hash: {type: String, required: false},
	email:{type: String, required: false},
	mobile: { type: String, required: false},
	address: {type: String, required: false},
	city: {type: String, required: false},
	state: {type: String, required: false},
	added_by_id: {type: String, required: false},
	added_by_name: {type: String, required: false},
	verifytoken:{type: String, required: false},
	lastvisited:{type: Date, required: false},
	login_status:{type: String, required: false}
},{
	timestamps: true
});


userSchema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('Users', userSchema);